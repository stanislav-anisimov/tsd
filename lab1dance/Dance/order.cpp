#include "order.h"

Queue::Queue()
{
	head = NULL;
	tail = NULL;
};

Queue::~Queue()
{
	while(this->head != NULL)
		this->Pop();
};

void Queue::Push(const QString  & str)
{
	int newSize = Size() + 1; //��������� ������ ������ ������� 

	char ** buffer = new char*[newSize]; //�������� ������ ��� ����� ������ ���������� �� ������
	char * newString = new char[str.size()+1]; //�������� ������ �� ����� ������

	const QChar * data = str.data(); //��������� ������ �� QString � char*
	for(int i = 0; !data->isNull(); i++){
		newString[i] = data->toAscii();
		data++;
	}
	newString[str.size()] = '\0';

	for (int i=0; i < newSize-1; i++) //������������ ��������� �� ������ �� ������� ������� � �����
		*(buffer + i*SIZE) = *(head + i*SIZE);

	if (head != NULL) //���� ���� ������ ������
		delete[] head; //������� ������ ������
	
	head = buffer; //�������������� ��������� �� ������ �������
	tail = buffer + (newSize-1)*SIZE; //��������� ��������� �� ��������� �������
	*(tail) = newString; //��������� � ������ ��������� �������
	
	int c = Size();
};

QString Queue::Pop()
{
	QString pop = "";

	if(head != NULL)
	{
		pop = QString(*head); 
		if(Size() > 1)
		{
			int newSize = Size() - 1; //��������� ������ ������ ������� 
			char ** buffer = new char* [newSize]; //�������� ������ ��� ����� ������ ���������� �� ������

			head += SIZE;

			for (int i=0; i < newSize; i++) //������������ ��������� �� ������ �� ������� ������� � �����
				*(buffer + i*SIZE) = *(head + i*SIZE);

			head -= SIZE;

			delete *head;

			delete head; //������� ������ ������
		
			head = buffer; //�������������� ��������� �� ������ �������
			tail = buffer + (newSize-1)*SIZE; //��������� ��������� �� ��������� �������
		}
		else
		{
			delete *head;
			delete head; //������� ������
			head = NULL;
			tail = NULL;
		}
	}
	return pop;
};

bool Queue::IsEmpty()
{
	return (head == NULL);
};

int Queue::Size()
{
	if(head == NULL)    return 0;
	return ((tail-head)/SIZE + 1);
};

QString CycleQueue::Pop()
{
	QString a = queue.Pop();
	queue.Push(a);
	return a;
};

void CycleQueue::Push(const QString & str)
{
	queue.Push(str);
}
