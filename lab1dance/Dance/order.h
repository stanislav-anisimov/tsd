#include <QFile>
#include <QString>
#include <locale.h>
#include <QTextCodec>
extern int SIZE;

class Queue
{
protected:
	char ** head;
	char ** tail;
public:
	Queue();
	~Queue();

	void Push(const QString &);
	QString Pop();
	bool IsEmpty();
	int Size();
};


class CycleQueue
{
public:
	Queue  queue;
	QString Pop();
	void Push(const QString &);
};



