#ifndef SHASH_H
#define SHASH_H
#define MAX_FILLED 0.5
#define EPS 0.001
#define MAX_SIZE 2147483648
#include <math.h>
#include <QDebug>

class SHash
{
public:
    SHash();
    ~SHash();
    bool addKey(int key);
    void removeKey(int key);
    bool getValue(int key);
    void clear();
    unsigned int getSize();
    void getCollisionMap(int* map);
    unsigned int getCollisions();
private:
    unsigned int size;
    unsigned int maxIndex;
    unsigned int filledCount;
    unsigned int collisionCount;
    int* data;
    bool* filled;
    unsigned int constMult;

    unsigned int hash(int key);
    bool isFilled();
    void expand();
};

#endif // SHASH_H
