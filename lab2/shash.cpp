#include "shash.h"

SHash::SHash()
{
    size=1048576;
    filledCount=0;
    data=new int[size];
    filled=new bool[size];
    for(unsigned int i=0;i<size;i++)
    {
        filled[i]=false;
    }
    constMult=pow(2,32)*(sqrt(5)-1)/2;
}
SHash::~SHash()
{
    delete[] data;
    delete[] filled;
}

unsigned int SHash::hash(int key)
{
    //Умножаю ключ на константу и беру первые n бит
    unsigned int result=(constMult*key) >> (int)(32-log2(size));
    return result;
}

bool SHash::addKey(int key)
{
    //qDebug() << "Adding key "+QString::number(key);
    if(filledCount==size)
    {
        //qDebug() << "Table full";
        return false;
    }
    //Если массив заполнен (коэфф заполненности MAX_FILLED)
    if(isFilled())
    {
        //qDebug() << "Table filled >50%";
        //Увеличиваем его размер
        expand();
    }
    //Считаем hash
    unsigned int index=hash(key);
    //qDebug() << "Key hash="+QString::number(index);
    //Ищем свободное место
    while(index<size && filled[index]==true)
    {
        index++;
        collisionCount++;
    }
    //Если нашли - вставляем
    if(index<size)
    {
        data[index]=key;
        filled[index]=true;
        filledCount++;
    }
    //Если не нашли - значит дошли до конца массива, начинаем искать в начале
    else
    {
        index=0;
        while(index<size && filled[index]==true)
        {
            index++;
            collisionCount++;
        }
        //Если нашли - вставляем, а должны были найти,
        //потому что место есть, чекали в начале функции
        if(index<size)
        {
            data[index]=key;
            filled[index]=true;
            filledCount++;
        }
        //Если ВДРУГ не нашли, возвращаем ложь
        else
        {
            return false;
        }
    }
    //qDebug() << "Added, filledCount="+QString::number(filledCount)+", collisions:"+QString::number(collisionCount);
    return true;
}

void SHash::removeKey(int key)
{
    //Считаем hash
    unsigned int index=hash(key);
    unsigned int initialIndex=index;
    //Ищем ключ
    while(index<size && data[index]!=key)
    {
        index++;
    }
    //Если нашли, удаляем
    if(index<size)
    {
        data[index]=0;
        filled[index]=false;
        filledCount--;
    }
    //Иначе ищем с начала массива, но останавливаемся, если дошли до места, откуда начинали
    else
    {
        index=0;
        while(index<initialIndex && data[index]!=key)
        {
            index++;
        }
        //Нашли - удаляем
        if(index<initialIndex)
        {
            data[index]=0;
            filled[index]=false;
            filledCount--;
        }
        //Иначе ничего не делаем, ключа и так нет
    }
}

bool SHash::getValue(int key)
{
    //Считаем hash
    unsigned int index=hash(key);
    unsigned int initialIndex=index;
    //Ищем ключ
    while(index<size && data[index]!=key)
    {
        index++;
    }
    //Если нашли, возвращаем тру
    if(index<size)
    {
        return true;
    }
    //Иначе ищем с начала массива, но останавливаемся, если дошли до места, откуда начинали
    else
    {
        index=0;
        while(index<initialIndex && data[index]!=key)
        {
            index++;
        }
        //Нашли - тру
        if(index<initialIndex)
        {
            return true;
        }
        //Иначе фолс
        else
        {
            return false;
        }
    }
}

bool SHash::isFilled()
{
    return (double)filledCount/size >= MAX_FILLED;
}

void SHash::expand()
{
     //qDebug() << "Expanding from "+QString::number(size);
    //Если достигли максимального размера, выходим из функции
    if(size==MAX_SIZE)
    {
        return;
    }
    //Запоминаем количество ключей
    unsigned int oldFilledCount=filledCount;
    //Заводим массив для записанных ключей
    int keys[filledCount];
    unsigned int keysIndex=0;
    //Для каждого элемента в хеше
    for(unsigned int i=0;i<size;i++)
    {
        //Если там записан ключ - переписываем ключ себе
        if(filled[i])
        {
            keys[keysIndex++]=data[i];
        }
    }
    //Удаляем старые массивы
    delete[] data;
    delete[] filled;
    //Новый размер в два раза больше, заводим новые массивы
    size*=2;
    filledCount=0;
    data=new int[size];
    filled=new bool[size];
    for(unsigned int i=0;i<size;i++)
    {
        filled[i]=false;
    }
    //Добавляем старые ключи в новый массив
    //qDebug()<<"========Adding old keys==========";
    for(unsigned int i=0;i<oldFilledCount;i++)
    {
        addKey(keys[i]);
    }
    //qDebug()<<"=================================";
}

void SHash::clear()
{
    //Удаляем старые массивы
    delete[] data;
    delete[] filled;
    //Заводим новые
    size=8;
    filledCount=0;
    data=new int[size];
    filled=new bool[size];
    for(unsigned int i=0;i<size;i++)
    {
        filled[i]=false;
    }
}

unsigned int SHash::getSize()
{
    return size;
}

void SHash::getCollisionMap(int *map)
{
    for(int i=0;i<size;i++)
    {
        map[i]=0;
        for(int j=0;j<size;j++)
        {
            if(filled[i] && filled[j] && hash(data[i])==hash(data[j]))
            {
                map[i]++;
            }
        }
    }
}

unsigned int SHash::getCollisions()
{
    unsigned int result=collisionCount;
    collisionCount=0;
    return result;
}
