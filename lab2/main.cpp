#include <QCoreApplication>
#include "shash.h"
#include <QDateTime>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    SHash hash;
    qsrand(QDateTime::currentMSecsSinceEpoch());
    QTime stopwatch;
    //all random
    qDebug()<<"==============1st test start==============";
    stopwatch.start();
    for(int i=0;i<10000;i++)
    {
        hash.addKey(2147483640);
        qDebug()<<QString::number(hash.getCollisions());
    }
    QString firstResult="Time: "+QString::number(stopwatch.elapsed()/1)+" msecs";
    qDebug()<<"==============1st test end==============";
    hash.clear();
    //50/50
    qDebug()<<"==============2nd test start==============";

    for(int i=0;i<50;i++)
    {
        hash.addKey(qrand());
    }
    stopwatch.start();
    for(int i=0;i<10000;i++)
    {
        hash.addKey(2147483640);
        qDebug()<<QString::number(hash.getCollisions());
    }
    QString secondResult="Time: "+QString::number(stopwatch.elapsed()/1)+" msecs";
    qDebug()<<"==============2nd test end==============";
    hash.clear();
    //all same
    qDebug()<<"==============3rd test start==============";
    for(int i=0;i<1000;i++)
    {
        hash.addKey(qrand());
    }
    stopwatch.start();
    for(int i=0;i<10000;i++)
    {
        hash.addKey(2147483640);
        qDebug()<<QString::number(hash.getCollisions());
    }
    QString thirdResult="Time: "+QString::number(stopwatch.elapsed()/1)+" msecs";
    qDebug()<<"==============3rd test end==============";
    hash.clear();
    qDebug()<<QString("First "+firstResult+", Second "+secondResult+", Third"+thirdResult);

//    //MEMLEAK TEST
//    SHash* newHash;
//    qDebug()<<"Starting memleak test";
//    for(;;)
//    {
//        newHash=new SHash();
//        newHash->addKey(qrand());
//        newHash->addKey(qrand());
//        newHash->addKey(qrand());
//        newHash->addKey(qrand());
//        delete newHash;
//    }
    return a.exec();
}
