QT += core
QT -= gui

CONFIG += c++11

TARGET = TSDlab2
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    shash.cpp

HEADERS += \
    shash.h
