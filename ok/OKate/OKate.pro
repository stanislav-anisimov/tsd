#-------------------------------------------------
#
# Project created by QtCreator 2016-11-07T18:08:14
#
#-------------------------------------------------

QT       += core gui
QT += network
QT += webenginewidgets
QT += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OKate
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    methodurlbuilder.cpp \
    checktokens.cpp

HEADERS  += mainwindow.h \
    methodurlbuilder.h \
    checktokens.h

FORMS    += mainwindow.ui \
    checktokens.ui

RESOURCES += \
    load.qrc
