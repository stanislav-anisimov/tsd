#ifndef METHODURLBUILDER_H
#define METHODURLBUILDER_H
#include <QString>
#include <QStringList>
#include <QMap>
#include <QCryptographicHash>
#include <QByteArray>
#include <QDebug>

class MethodUrlBuilder
{
private:
    QString app_key;
    QString access_token;
    QString format;
    QString _online;
    QString session_secret_key;
    QString method;
    QStringList params;
    QString sig;
public:
    MethodUrlBuilder(QString method, QStringList params, QString access_token, QString session_secret_key, QString format)
    {
        app_key="CBADEMGLEBABABABA";
        this->format=format;
        _online="true";
        this->method=method;
        this->params=params;
        this->access_token=access_token;
        this->session_secret_key=session_secret_key;
    }

    QString getUrl(QString hostname="https://api.ok.ru/fb.do?", bool noSig=false, bool paramsOnly=false)
    {
        QString result=hostname;
        if(!paramsOnly)
        {
            params.append("application_key="+app_key);
            params.append("format="+format);
            params.append("method="+method);
            params.append("_online="+_online);
            params.sort();
            QString gluedParams=params.join("");
            gluedParams.append(session_secret_key);
            sig=QCryptographicHash::hash(gluedParams.toStdString().c_str(),QCryptographicHash::Md5).toHex();
            result.append("access_token="+access_token);
            if(!noSig)
            {
                result.append("&sig="+sig);
            }
        }
        for(int i=0;i<params.length();i++)
        {
            result.append("&"+params[i]);
        }
        return result;
    }
};

#endif // METHODURLBUILDER_H
