#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/qnetworkaccessmanager.h>
#include <QtNetwork/qnetworkreply.h>
#include <QtNetwork/qnetworkrequest.h>
#include <QUrl>
#include <QDateTime>
#include <QWebEngineView>
#include <QMap>
#include "methodurlbuilder.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QThread>
#include <QFile>
#include <QDataStream>
#include <QIODevice>
#include <QTimer>
#include <QMovie>
#include <QXmlStreamReader>
#include "checktokens.h"

const QString SERVERNAME = QString("http://localhost");
const QString SAVEDDB = QString(".\\db.OKate");

namespace Ui {
class MainWindow;
}

class Message
{
private:
    friend QDataStream & operator<<(QDataStream &out,   const Message & data);
    friend QDataStream & operator>>(QDataStream &in,          Message & data);
public:
    QString author;
    QString text;
    QDateTime time;
    Message(QString author, QDateTime &time, QString &text);
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    QWebEngineView oauthPage;
    Ui::MainWindow *ui;
    QNetworkAccessManager* nwdriver;
    QMap<QString,QString> parseAuthParams(QString raw);
    QMap<QString,QString> authParams;


    QMap<QString, QList<Message> > db;
    QString currentUserId;

    int countOfLoadedFriends;   //количество загруженных уже друзей
    int deltaFriends;   //количество друзей которые будем дозагружать

    QNetworkReply* sdkTokenReply;
    QString sdkToken;
    //void(MainWindow::*nextToExecute)();
    QString nextToExecute;

    QTimer *timer; //таймер для сохранения диалогов
    QTimer *messageRefreshTimer;
    QStringList friendsList;
    QStringList friends5;

    bool alreadyLoadingMoreFriends;

    QString format;
    void isLoginedBlock(bool logined);
    bool saveDb();
    bool loadDb();
    friend QDataStream & operator<<(QDataStream & out,  const   QMap<QString, QList<Message>> & data);
    friend QDataStream & operator>>(QDataStream & in,           QMap<QString, QList<Message>> & data);
    friend QDataStream & operator<<(QDataStream & out,  const   QList<Message> & data);
    friend QDataStream & operator>>(QDataStream & in,           QList<Message> & data);

private slots:
    void login();
    void loadLoginData(QUrl url);
    void handleReply(QNetworkReply* reply);
    void loadMessages();
    void checkForNewMessages(int row, int col);
    void checkForNewMessagesOnTimer();
    void getNotes();
    void sendMessage();
    void sendNote();
    void refresh();
    void resaveDb();
    void relogin();
    void reloadFriends();
    void checkTokens();
    void wait(QString message);
    void release();
    void changeFormat();
};

class LoadAvatarsThread : public QThread
{
    Q_OBJECT
private:
    QNetworkAccessManager *nwdriver;
    QList<QNetworkReply*> avatars;
    QStringList urlsStr;
    Ui::MainWindow *ui;
public:
    LoadAvatarsThread(QStringList urlsStr,Ui::MainWindow *ui);
    void run();
    int countOfLoadedFriends;
    int deltaFriends;
private slots:
    void loadAvatar(QNetworkReply*);
};



#endif // MAINWINDOW_H
