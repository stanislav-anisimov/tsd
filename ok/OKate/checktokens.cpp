#include "checktokens.h"
#include "ui_checktokens.h"

CheckTokens::CheckTokens(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CheckTokens)
{
    nwdriver=new QNetworkAccessManager();
    ui->setupUi(this);
    connect(ui->check,SIGNAL(clicked(bool)),this,SLOT(check()));
    connect(nwdriver,SIGNAL(finished(QNetworkReply*)),this,SLOT(handleReply(QNetworkReply*)));
}

CheckTokens::~CheckTokens()
{
    delete ui;
}

void CheckTokens::setData(QString token, QString key)
{
    ui->access_token->setText(token);
    ui->secret_key->setText(key);
}

void CheckTokens::check()
{
    QString url="http://";
    url.append(ui->ip->text()).append("/checkTokens?token=").append(ui->access_token->text()).append("&key=").append(ui->secret_key->text());
    QNetworkRequest req=QNetworkRequest(QUrl(url));
    QNetworkReply *reply=nwdriver->get(req);
}

void CheckTokens::handleReply(QNetworkReply *reply)
{
    ui->response->setText(QString(reply->readAll()));
}
