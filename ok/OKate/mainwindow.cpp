#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "Windows.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    alreadyLoadingMoreFriends=false;
    ui->chat->setReadOnly(true);
    QMovie *loadGif=new QMovie(":/load/loadGif");
    if(loadGif->isValid())
    {
       ui->animation->setMovie(loadGif);
       loadGif->start();
       ui->animation->hide();
    }
    oauthPage.setUrl(QUrl("https://connect.ok.ru/oauth/authorize?client_id=1248543232&scope=VALUABLE_ACCESS&response_type=token&redirect_uri=https://api.ok.ru/blank.html&layout=a"));
    timer = new QTimer;
    messageRefreshTimer=new QTimer;
    deltaFriends = 5;
    countOfLoadedFriends = 0;
    ui->friends->setRowCount(0);
    ui->friends->setColumnCount(3);
    ui->friends->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->friends->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    ui->friends->verticalHeader()->hide();

    ui->friends->horizontalHeader()->hide();

    nwdriver=new QNetworkAccessManager();
    this->loadDb();
    connect(ui->login,SIGNAL(clicked(bool)),this,SLOT(login()));
    connect(&oauthPage,SIGNAL(urlChanged(QUrl)),this,SLOT(loadLoginData(QUrl)));
    connect(nwdriver,SIGNAL(finished(QNetworkReply*)),this,SLOT(handleReply(QNetworkReply*)));
    connect(ui->friends,SIGNAL(cellClicked(int,int)),this,SLOT(checkForNewMessages(int,int)));
    connect(ui->jsonButton,SIGNAL(toggled(bool)),this,SLOT(changeFormat()));
    connect(ui->sendMessageButton,SIGNAL(clicked(bool)),this,SLOT(sendMessage()));
    connect(ui->message,SIGNAL(returnPressed()),this,SLOT(sendMessage()));
    connect(timer, SIGNAL(timeout()), this, SLOT(resaveDb()));
    connect(messageRefreshTimer,SIGNAL(timeout()),this,SLOT(checkForNewMessagesOnTimer()));
    connect(ui->checkTokens,SIGNAL(clicked(bool)),this,SLOT(checkTokens()));
    isLoginedBlock(false);
    timer->start(3000);
    messageRefreshTimer->start(1000);
}

void MainWindow::handleReply(QNetworkReply* reply)
{
    QString query=reply->request().url().query();
    if (format=="JSON")
    {
        QJsonDocument replyJSON=QJsonDocument::fromJson(reply->readAll());
        if(replyJSON.isObject() && replyJSON.object().contains("error_code"))
        {
            qDebug() << replyJSON.toJson().toStdString().c_str();
            if(replyJSON.object().value("error_code").toInt()==102)
            {
                relogin();
            }
            return;
        }
        if(query.contains("friends.get"))
        {
            //loadFriendsIds
            friendsList=replyJSON.toVariant().toStringList();
            QStringList params;
            release();
            params.append("fields=NAME,PIC128X128,BIRTHDAY");
            for (int i = 0; i < deltaFriends  &&  i<friendsList.size()-countOfLoadedFriends;i++)
            {
                friends5.append(friendsList.at(countOfLoadedFriends+i));
            }
            params.append("uids="+friends5.join(','));
            MethodUrlBuilder builder("users.getInfo",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
            QNetworkRequest req(QUrl(builder.getUrl()));
            QNetworkReply *friendsInfoReply=nwdriver->get(req);
            wait("Loading friends info");
        }
        else if(query.contains("users.getInfo"))
        {
            //loadFriendsInfo
            release();
            wait("Loading friends info");
            QStringList avatarUrls;
            if(countOfLoadedFriends == 0)
            {
                ui->friends->insertRow(ui->friends->rowCount());
            }
            for(int i=0;i<replyJSON.array().size() ;i++)
            {
                ui->friends->insertRow(ui->friends->rowCount());
                QTableWidgetItem *item=new QTableWidgetItem;
                item->setText(replyJSON.array().at(i).toObject().value("name").toString());
                item->setData(Qt::UserRole,replyJSON.array().at(i).toObject().value("uid").toString());
                ui->friends->setItem(countOfLoadedFriends,1,item);
                item=new QTableWidgetItem;
                item->setText(replyJSON.array().at(i).toObject().value("birthday").toString());
                ui->friends->setItem(countOfLoadedFriends,2,item);
                avatarUrls.append(replyJSON.array().at(i).toObject().value("pic128x128").toString());
                countOfLoadedFriends++;
            }
            if(friends5.count() != replyJSON.array().size())
            {
                QStringList correctUID;
                for (int i=0;i<replyJSON.array().size() ;i++)
                {
                    correctUID.append(replyJSON.array().at(i).toObject().value("uid").toString());
                }
                for(int i=0;i<friends5.count() ;i++)
                {
                    if(!correctUID.contains((friends5.at(i))))
                        friendsList.removeAll(friends5.at(i));
                }
            }
            if(countOfLoadedFriends == friendsList.count())
                ui->friends->removeRow(ui->friends->rowCount()-1);
            else
            {
                QTableWidgetItem *item=new QTableWidgetItem;
                item->setText("Загрузить еще...");
                ui->friends->setItem(countOfLoadedFriends, 1, item);
            }
            release();
            LoadAvatarsThread *th=new LoadAvatarsThread(avatarUrls,ui);
            th->countOfLoadedFriends=countOfLoadedFriends;
            th->deltaFriends=replyJSON.array().size();
            th->run();
            alreadyLoadingMoreFriends=false;
        }
        else if(query.contains("sdk.init"))
        {
            release();
            //init then do sdk method
            sdkToken=replyJSON.object().value("session_key").toString();
            QMetaObject::invokeMethod(this,nextToExecute.toStdString().c_str(),Qt::DirectConnection);
        }
        else if(query.contains("sdk.getNotes"))
        {
            //load new msgs to db
            for(int i=0;i<replyJSON.object().value("notes").toArray().size();i++)
            {
                QString uid=replyJSON.object().value("notes").toArray().at(i).toObject().value("uid").toString();
                if(!db.contains(uid))
                {
                    db.insert(uid,QList<Message>());
                }
                //DateTime must be extracted from note!!!!!
                QDateTime msgTime=QDateTime::fromTime_t(QByteArray::fromBase64(QByteArray(replyJSON.object().value("notes").toArray().at(i).toObject().value("payload").toString().toStdString().c_str())).toUInt()).toLocalTime();
                Message msg(uid,msgTime,replyJSON.object().value("notes").toArray().at(i).toObject().value("message").toString().remove(0,16));
                bool isUnique=true;
                for(int i=0;i<db.value(uid).size();i++)
                {
                    if(db.value(uid).at(i).time==msg.time)
                    {
                        isUnique=false;
                    }
                }
                if(isUnique)
                {
                    db[uid].append(msg);
                }
            }
            QStringList params;
            params.append(QString("sdkToken=").append(sdkToken));
            params.append("timestamp=9999999999999");
            MethodUrlBuilder builder("sdk.resetNotes",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
            nwdriver->get(QNetworkRequest(QUrl(builder.getUrl())));
            loadMessages();
        }
        else if(query.contains("users.getCurrentUser"))
        {
            release();
            currentUserId=replyJSON.object().value("uid").toString().toLong();
            ui->access_token->appendPlainText(QString("\nLogged in as ").append(replyJSON.object().value("name").toString()));
        }
        else if(query.contains("sdk.sendNote"))
        {
            Message msg(this->currentUserId,QDateTime::currentDateTime(),ui->message->text());
            QString currentFriendId=ui->friends->item(ui->friends->currentRow(),1)->data(Qt::UserRole).toString();
            if(!db.contains(currentFriendId))
            {
                db.insert(currentFriendId,QList<Message>());

            }
            db[currentFriendId].append(msg);
            loadMessages();
            ui->message->clear();
            qDebug() << replyJSON.toBinaryData().toStdString().c_str();
        }
    }
    else if (format=="XML")
    {
        QXmlStreamReader xmlReader;
        xmlReader.addData(reply->readAll());

        if(query.contains("friends.get"))
        {
//            //loadFriendsIds
            while (!xmlReader.atEnd())
            {
                QString nameOfTag = xmlReader.name().toString();
                if (nameOfTag=="uid" && !xmlReader.isEndElement())
                {
                    xmlReader.readNext();
                    friendsList.append(xmlReader.text().toString());
                }
                xmlReader.readNext();
            }
            QStringList params;
            release();
            params.append("fields=NAME,PIC128X128,BIRTHDAY");
            for (int i = 0; i < deltaFriends  &&  i<friendsList.size()-countOfLoadedFriends;i++)
            {
                friends5.append(friendsList.at(countOfLoadedFriends+i));
            }
            params.append("uids="+friends5.join(','));
            MethodUrlBuilder builder("users.getInfo",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
            QNetworkRequest req(QUrl(builder.getUrl()));
            QNetworkReply *friendsInfoReply=nwdriver->get(req);
            wait("Loading friends info");
        }
        else if(query.contains("users.getInfo"))
        {
            int friendsCountInReply=0;
            //loadFriendsInfo
            release();
            QStringList avatarUrls;
            if(countOfLoadedFriends == 0)
            {
                ui->friends->insertRow(ui->friends->rowCount());
            }
            QString uid,birthday,name,pic;
            QStringList UIDs;
            while (!xmlReader.atEnd())
            {
                QString nameOfTag = xmlReader.name().toString();
                if (nameOfTag=="user" && !xmlReader.isEndElement())
                {
                    xmlReader.readNext();
                    xmlReader.readNext();
                    uid=xmlReader.text().toString();
                    UIDs.append(uid);
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    birthday=xmlReader.text().toString();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    name=xmlReader.text().toString();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    pic=xmlReader.text().toString();
                    ui->friends->insertRow(ui->friends->rowCount());
                    QTableWidgetItem *item=new QTableWidgetItem;
                    item->setText(name);
                    item->setData(Qt::UserRole,uid);
                    ui->friends->setItem(countOfLoadedFriends,1,item);
                    item=new QTableWidgetItem;
                    item->setText(birthday);
                    ui->friends->setItem(countOfLoadedFriends,2,item);
                    avatarUrls.append(pic);
                    countOfLoadedFriends++;
                    friendsCountInReply++;
                }
                xmlReader.readNext();
            }

            if(friends5.count() != friendsCountInReply)
            {
                QStringList correctUID;
                for (int i=0;i<friendsCountInReply ;i++)
                {
                    correctUID.append(UIDs[i]);
                }
                for(int i=0;i<friends5.count() ;i++)
                {
                    if(!correctUID.contains((friends5.at(i))))
                        friendsList.removeAll(friends5.at(i));
                }
            }
            if(countOfLoadedFriends == friendsList.count())
                ui->friends->removeRow(ui->friends->rowCount()-1);
            else
            {
                QTableWidgetItem *item=new QTableWidgetItem;
                item->setText("Загрузить еще...");
                ui->friends->setItem(countOfLoadedFriends, 1, item);
            }
            release();
            LoadAvatarsThread *th=new LoadAvatarsThread(avatarUrls,ui);
            th->countOfLoadedFriends=countOfLoadedFriends;
            th->deltaFriends=friendsCountInReply;
            th->run();
            alreadyLoadingMoreFriends=false;
        }
        else if(query.contains("sdk.init"))
        {
            while (!xmlReader.atEnd())
            {
                QString nameOfTag = xmlReader.name().toString();
                if (nameOfTag=="session_key" && !xmlReader.isEndElement())
                {
                    xmlReader.readNext();
                    sdkToken=xmlReader.text().toString();
                }
                xmlReader.readNext();
            }
            //init then do sdk method
            QMetaObject::invokeMethod(this,nextToExecute.toStdString().c_str(),Qt::DirectConnection);
        }
        else if(query.contains("sdk.getNotes"))
        {
            QString message,payload,uid;
            while (!xmlReader.atEnd())
            {
                QString nameOfTag = xmlReader.name().toString();
                if (nameOfTag=="note" && !xmlReader.isEndElement())
                {
                    xmlReader.readNext();
                    xmlReader.readNext();
                    uid=xmlReader.text().toString();
                    if(!db.contains(uid))
                    {
                        db.insert(uid,QList<Message>());
                    }
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    message=xmlReader.text().toString();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    xmlReader.readNext();
                    payload=xmlReader.text().toString();
                    QDateTime msgTime=QDateTime::fromTime_t(QByteArray::fromBase64(QByteArray(payload.toStdString().c_str())).toUInt()).toLocalTime();
                    Message msg(uid,msgTime,message.remove(0,16));
                    bool isUnique=true;
                    for(int i=0;i<db.value(uid).size();i++)
                    {
                        if(db.value(uid).at(i).time==msg.time)
                        {
                            isUnique=false;
                        }
                    }
                    if(isUnique)
                    {
                        db[uid].append(msg);
                    }
                }
                xmlReader.readNext();
            }
            QStringList params;
            params.append(QString("sdkToken=").append(sdkToken));
            params.append("timestamp=9999999999999");
            MethodUrlBuilder builder("sdk.resetNotes",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
            nwdriver->get(QNetworkRequest(QUrl(builder.getUrl())));
            loadMessages();
        }
        else if(query.contains("users.getCurrentUser"))
        {
            release();
            while (!xmlReader.atEnd())
            {
                QString nameOfTag = xmlReader.name().toString();
                if (nameOfTag=="uid" && !xmlReader.isEndElement())
                {
                    xmlReader.readNext();
                    currentUserId=xmlReader.text().toString();
                }
                if (nameOfTag=="name" && !xmlReader.isEndElement())
                {
                    xmlReader.readNext();
                    currentUserId=xmlReader.text().toString();
                    ui->access_token->appendPlainText(QString("\nLogged in as ").append(xmlReader.text().toString()));
                }
                xmlReader.readNext();
            }
        }
        else if(query.contains("sdk.sendNote"))
        {
            Message msg(this->currentUserId,QDateTime::currentDateTime(),ui->message->text());
            QString currentFriendId=ui->friends->item(ui->friends->currentRow(),1)->data(Qt::UserRole).toString();
            if(!db.contains(currentFriendId))
            {
                db.insert(currentFriendId,QList<Message>());

            }
            db[currentFriendId].append(msg);
            loadMessages();
            ui->message->clear();
        }
    }
}

void MainWindow::loadMessages()
{
    ui->chat->clear();
    QString friendId=ui->friends->item(ui->friends->currentRow(),1)->data(Qt::UserRole).toString();
    QList<Message> messages;
    if(db.contains(friendId))
    {
        messages=db.value(friendId);
    }
    for(int i=0;i<messages.size();i++)
    {
        ui->chat->append(messages[i].time.toString("HH:mm").append(" "));
        if(messages[i].author==currentUserId)
        {
            ui->chat->append("You: ");
        }
        else
        {
            ui->chat->append(QString(messages[i].author).append(":"));
        }
        ui->chat->append(QString(messages[i].text).append("\n"));
    }
}

void MainWindow::checkForNewMessages(int row, int col)
{

    if(!alreadyLoadingMoreFriends && ui->friends->currentRow() == countOfLoadedFriends)
    {
        alreadyLoadingMoreFriends=true;
        reloadFriends();
        return;
    }
    QStringList params;
    params.append("session_data={\"version\":2,\"device_id\":\"48569845e2\",\"client_version\":\"js_sdk_1\",\"client_type\":\"SDK_JS\"}");
    params.append("application_key=CBADEMGLEBABABABA");
    if (ui->jsonButton->isChecked())
        params.append("format=json");
    else
        params.append("format=XML");
    params.append("method=sdk.init");
    MethodUrlBuilder builder("sdk.init",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
    nextToExecute=QString("getNotes");
    QNetworkReply *reply=nwdriver->get(QNetworkRequest(QUrl(builder.getUrl("https://api.ok.ru/fb.do?",true,true))));
}

void MainWindow::checkForNewMessagesOnTimer()
{
    if(ui->friends->currentRow()!=-1)
    {
        checkForNewMessages(ui->friends->currentRow(),0);
    }
}


void MainWindow::getNotes()
{
    QStringList params;
    params.append(QString("sdkToken=").append(sdkToken));
    MethodUrlBuilder builder("sdk.getNotes",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
    QNetworkReply *reply=nwdriver->get(QNetworkRequest(QUrl(builder.getUrl())));
    nextToExecute="";
}

void MainWindow::sendMessage()
{
    if(!ui->message->text().isEmpty() && (ui->friends->currentRow() != -1)) {
        QStringList params;
        params.append("session_data={\"version\":2,\"device_id\":\"48569845e2\",\"client_version\":\"js_sdk_1\",\"client_type\":\"SDK_JS\"}");
        params.append("application_key=CBADEMGLEBABABABA");
        if (ui->jsonButton->isChecked())
            params.append("format=json");
        else
            params.append("format=XML");
        params.append("method=sdk.init");
        MethodUrlBuilder builder("sdk.init",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
        nextToExecute=QString("sendNote");
        QNetworkReply *reply=nwdriver->get(QNetworkRequest(QUrl(builder.getUrl("https://api.ok.ru/fb.do?",true,true))));
    }
}

void MainWindow::sendNote()
{
    QStringList params;
    params.append(QString("sdkToken=").append(sdkToken));
    QByteArray currentTime=QByteArray::fromStdString(QString::number(QDateTime::currentDateTime().toTime_t()).toStdString());
    currentTime=currentTime.toBase64();
    QString note("note={\"message\":\"");
    note.append(QString("1234567891113016").append(ui->message->text())).append("\",\"uid\":")
            .append(ui->friends->item(ui->friends->currentRow(),1)->data(Qt::UserRole).toString())
            .append(",\"image\":\"http://yastatic.net/morda-logo/i/ya_favicon_ru.png\",\"payload\":\"")
            .append(currentTime.toStdString().c_str()).append("\"}");
    params.append(note);
    MethodUrlBuilder builder("sdk.sendNote",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
    QNetworkReply *reply=nwdriver->get(QNetworkRequest(QUrl(builder.getUrl())));
    nextToExecute="";
}

void MainWindow::refresh()
{
    checkForNewMessages(0,0);
}


void MainWindow::login()
{
    wait("Logging in");
   if (ui->jsonButton->isChecked())
       format="JSON";
   else
       format="XML";  
   oauthPage.show();
}

LoadAvatarsThread::LoadAvatarsThread(QStringList urlsStr, Ui::MainWindow *ui)
{
    this->urlsStr=urlsStr;
    this->ui=ui;
    nwdriver=new QNetworkAccessManager();
    connect(nwdriver,SIGNAL(finished(QNetworkReply*)),this,SLOT(loadAvatar(QNetworkReply*)));
}

void LoadAvatarsThread::run()
{
    for(int i=0;i<urlsStr.size();i++)
    {
        QNetworkReply* reply=nwdriver->get(QNetworkRequest(QUrl(urlsStr[i])));
    }
}

void LoadAvatarsThread::loadAvatar(QNetworkReply *reply)
{
   int index=urlsStr.indexOf(reply->request().url().toString());

    QTableWidgetItem* item=new QTableWidgetItem;
    QPixmap avatar;
    avatar.loadFromData(reply->readAll());
    avatar=avatar.scaled(QSize(128,128));
    item->setData(Qt::DecorationRole,avatar);
    if(countOfLoadedFriends-deltaFriends>0)
        ui->friends->setItem(countOfLoadedFriends-deltaFriends+index,0,item);
    else
        ui->friends->setItem(index,0,item);
}

void MainWindow::loadLoginData(QUrl url)
{
    if(url.host()=="api.ok.ru")
    {
        authParams=parseAuthParams(url.fragment());
        QString str;
        for(QMap<QString,QString>::iterator it=authParams.begin();it!=authParams.end();it++)
        {
            str.append(it.key()).append(":").append(it.value()).append("\n");
        }
        ui->access_token->setPlainText(str);
        QStringList params;
        MethodUrlBuilder urlBuilder("friends.get",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
        QNetworkRequest req(QUrl(urlBuilder.getUrl()));
        QNetworkReply *friendsReply=nwdriver->get(req);
        params.append("fields=NAME,UID");
        MethodUrlBuilder urlBuilder2("users.getCurrentUser",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
        QNetworkReply *reply=nwdriver->get(QNetworkRequest(QUrl(urlBuilder2.getUrl())));
        oauthPage.stop();
        oauthPage.hide();
        isLoginedBlock(true);
    }
}

QMap<QString,QString> MainWindow::parseAuthParams(QString raw)
{
    QMap<QString,QString> result;
    QStringList fields=raw.split('&');
    for(int i=0;i<fields.size();i++)
    {
        QStringList entry=fields[i].split('=');
        result.insert(entry[0],entry[1]);
    }
    return result;
}

void MainWindow::isLoginedBlock(bool logined) {
    ui->login->setEnabled(!logined);
    ui->sendMessageButton->setEnabled(logined);
    ui->message->setEnabled(logined);
    ui->friends->setEnabled(logined);
}

void MainWindow::resaveDb() {
    saveDb();
}

void MainWindow::relogin()
{
    isLoginedBlock(false);
    wait("Please relogin");
}

bool MainWindow::saveDb() {
    QFile file(SAVEDDB);
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream output(&file);
        output << this->db;
        file.close();
        return true;
    }
    return false;
}

bool MainWindow::loadDb() {
    QFile file(SAVEDDB);
    if (file.open(QIODevice::ReadOnly)) {
        QDataStream input(&file);
        input >> this->db;
        file.close();
        return true;
    }
    return false;
}

QDataStream & operator<<(QDataStream & out, const QMap<QString, QList<Message>> & data) {
    out << data.size();
    QMap<QString, QList<Message>>::const_iterator i = data.constBegin();
    while (i != data.constEnd())
    {	out << i.key() << i.value();
        ++i;
    }
    return out;
}

QDataStream & operator>>(QDataStream &in, QMap<QString, QList<Message>> & data) {
    int size;
    QString key;
    QList<Message> value;
    in >> size;
    for(int i = 0; i < size; i++)
    {	in >> key;
        in >> value;
        data.insert(key,value);
        value.clear();
    }
    return in;
}

QDataStream & operator<<(QDataStream & out, const QList<Message> & data) {
    out << data.size();
    QList<Message>::const_iterator i = data.constBegin();
    while (i != data.constEnd())
    {	out << *i;
        ++i;
    }
    return out;
}

QDataStream & operator>>(QDataStream & in, QList<Message> & data)
{
    int size;
    QString author;
    QDateTime time;
    QString text;
    in >> size;
    for(int i = 0; i < size; i++)
    {	in >> author;
        in >> time;
        in >> text;
        data << Message(author, time, text);
    }
    return in;
}


MainWindow::~MainWindow()
{
    delete ui;
}

Message::Message(QString author, QDateTime& time, QString& text)
{
    this->author=author;
    this->text=text;
    this->time=time;
}

QDataStream & operator<<(QDataStream &out, const Message & data)
{
    out << data.author << data.time << data.text;
    return out;
}

QDataStream & operator>>(QDataStream &in, Message & data)
{
    in >>  data.author >>  data.time >>  data.text;
    return in;
}
void MainWindow::reloadFriends()
{

        QStringList params;
        friends5.clear();
        params.append("fields=NAME,PIC128X128,BIRTHDAY");
        for (int i = 0; i<deltaFriends &&  i<friendsList.size()-countOfLoadedFriends;i++)
        {
            friends5.append(friendsList.at(countOfLoadedFriends+i));
        }
        params.append("uids="+friends5.join(','));

        MethodUrlBuilder builder("users.getInfo",params,authParams.value("access_token"),authParams.value("session_secret_key"),format);
        QNetworkRequest req(QUrl(builder.getUrl()));
        QNetworkReply *friendsInfoReply=nwdriver->get(req);

}


void MainWindow::checkTokens()
{
    CheckTokens *w=new CheckTokens();
    w->setData(authParams.value("access_token"),authParams.value("session_secret_key"));
    w->show();
}

void MainWindow::wait(QString message)
{
    ui->status->setText(message);
    ui->animation->show();
    ui->jsonButton->setEnabled(false);
    ui->xmlButton->setEnabled(false);
}

void MainWindow::release()
{
    ui->status->setText("");
    ui->animation->hide();
    ui->jsonButton->setEnabled(true);
    ui->xmlButton->setEnabled(true);
}

void MainWindow::changeFormat()
{
    if(ui->jsonButton->isChecked())
    {
        format="JSON";
    }
    else
    {
        format="XML";
    }
}

