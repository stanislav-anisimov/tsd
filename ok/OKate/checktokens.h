#ifndef CHECKTOKENS_H
#define CHECKTOKENS_H

#include <QMainWindow>
#include <QtNetwork/qnetworkaccessmanager.h>
#include <QtNetwork/qnetworkreply.h>
#include <QtNetwork/qnetworkrequest.h>
#include <QUrl>

namespace Ui {
class CheckTokens;
}

class CheckTokens : public QMainWindow
{
    Q_OBJECT

public:
    explicit CheckTokens(QWidget *parent = 0);
    ~CheckTokens();

public slots:
    void setData(QString token, QString key);
    void check();
    void handleReply(QNetworkReply*reply);

private:
    Ui::CheckTokens *ui;
    QNetworkAccessManager* nwdriver;
};

#endif // CHECKTOKENS_H
