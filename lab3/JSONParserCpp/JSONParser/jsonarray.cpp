#include "jsonarray.h"

void JSONArray::parse(std::string& json, int &currentIndex)
{
    //QTime timer;
    //timer.start();
    int length=json.length();
    while(currentIndex<length && json.at(currentIndex)!=']')
    {
        currentIndex++;
        //totalMsecsArr+=timer.elapsed();
        JSONType *value=JSONParser::parseController(json, currentIndex);
        //timer.restart();
        this->append(value);
    }
    currentIndex++;
    //totalArr++;
    //totalMsecsArr+=timer.elapsed();
}
