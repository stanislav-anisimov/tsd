#ifndef JSONBOOL_H
#define JSONBOOL_H
#include "jsontype.h"


class JSONBool:public JSONType
{
private:
    bool val;
public:
    JSONBool();
    void parse(std::string& json, int &currentIndex)
    {
        if(json.at(currentIndex)=='t')
        {
            this->val=true;
            currentIndex+=4;
        }
        else
        {
            this->val=false;
            currentIndex+=5;
        }
    }
    std::string toString()
    {
        std::string result;
        val?result="true":result="false";
        return result;
    }

};

#endif // JSONBOOL_H
