#ifndef JSONOBJECT_H
#define JSONOBJECT_H
#include "jsontype.h"
#include "jsonstring.h"
#include "jsonparser.h"
#include <map>
#include <algorithm>
#include <iterator>


class JSONObject:public JSONType
{
private:
    std::map<std::string, JSONType*> fields;
public:
    JSONObject()
    {
    }

  void parse(std::string& json, int &currentIndex);

    ~JSONObject()
    {
        for(std::map<std::string, JSONType*>::iterator it=fields.begin();it!=fields.end();it++)
        {
            delete it->second;
        }
        fields.clear();
    }

    JSONType* get(char* field)
    {
       JSONType* result;
       std::map<std::string,JSONType*>::iterator fieldIterator=fields.find(std::string(field));
       if(fieldIterator!=fields.end())
       {
           result=fieldIterator->second;
       }
       else
       {
           result=0;
       }
       return result;
    }


    std::string toString()
    {
       std::string result="{";
       std::vector<std::string> keys;
       for(std::map<std::string,JSONType*>::iterator it=fields.begin();it!=fields.end();it++)
       {
           keys.push_back(it->first);
       }
       for(std::vector<std::string>::iterator it=keys.begin();it!=keys.end();it++)
       {
           result+=*it+" : "+fields[*it]->toString()+",";
       }
       result+="}";
       return result;
    }

    JSONObject* append(std::string field,JSONType* value)
    {
        fields.insert(std::pair<std::string,JSONType*>(field,value));
        return this;
    }
};

#endif // JSONOBJECT_H
