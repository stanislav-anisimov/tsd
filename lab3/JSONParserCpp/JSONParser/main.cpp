#include <QCoreApplication>
#include <jsonparser.h>
#include <iostream>
#include <fstream>
#include <QTime>
#include <streambuf>

int main(int argc, char *argv[])
{
    std::ifstream input;
    if(argc==2)
    {
        input.open(argv[1],std::fstream::in);
    }
    else
    {
        input.open("input1.json",std::ifstream::in);
    }
    std::string *str=new std::string(std::istreambuf_iterator<char>(input), std::istreambuf_iterator<char>());
    bool ok=input.is_open();
    qDebug() << str->size() << " bytes read";
    QTime timer;
    timer.start();
    JSONType* obj=JSONParser::parse(*str);
    int time=timer.elapsed();
  //  qDebug() << obj->toString().c_str();
    delete str;
    std::cout << "Total time " << (double)time/1000 << "sec";
    //JSONNumber* num=dynamic_cast<JSONNumber*>(obj->get(12)->get("index"));
   // int kek=num->value();
    return 0;
}
