#include "jsonobject.h"

void JSONObject::parse(std::string& json, int &currentIndex)
  {
      //QTime timer;
      //timer.start();
      int length=json.length();
      while(currentIndex<length && json.at(currentIndex)!='}')
      {
          currentIndex++;
          std::string key;
          if(json.at(currentIndex)=='\"' || json.at(currentIndex)=='\'')
          {
              //totalMsecsObj+=timer.elapsed();
              JSONString temp;
              temp.parse(json,currentIndex);
              //timer.restart();
              key=temp.toString();
              currentIndex++;
          }
          else
          {
              int newCurrentIndex=json.find(':',currentIndex);
              std::string key=json.substr(currentIndex,newCurrentIndex-currentIndex-1);
              currentIndex=newCurrentIndex+1;
          }
         // totalMsecsObj+=timer.elapsed();
          JSONType* value=JSONParser::parseController(json,currentIndex);
          //timer.restart();
          this->append(key,value);
      }
      currentIndex++;
      //totalObj++;
      //totalMsecsObj+=timer.elapsed();
  }
