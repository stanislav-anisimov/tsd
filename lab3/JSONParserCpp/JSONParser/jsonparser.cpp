#include "jsonparser.h"

int JSONParser::currentIndex=0;
int JSONParser::totalMsecsArr=0;
int JSONParser::totalMsecsObj=0;
int JSONParser::totalMsecsVal=0;
int JSONParser::totalMsecsControl=0;
int JSONParser::totalMsecsString=0;
int JSONParser::totalMsecsNumber=0;

int JSONParser::totalArr=0;
int JSONParser::totalObj=0;
int JSONParser::totalVal=0;
int JSONParser::totalControl=0;
int JSONParser::totalString=0;
int JSONParser::totalNumber=0;

int JSONParser::percent=0;

JSONType* JSONParser::parseController(std::string& json, int &currentIndex)
{
    //QTime timer;
    //timer.start();
    if((double)currentIndex/json.size()*100-percent>=10)
    {
        percent+=10;
        qDebug("%d%%\t",percent);
    }
    JSONType *obj;
    char ch=json.at(currentIndex);

    if(ch=='{')
    {
        //totalMsecsControl+=timer.elapsed();
        obj = new JSONObject();
        //obj=dynamic_cast<JSONObject*>(obj);
        obj->parse(json,currentIndex);
        //timer.restart();
    }
    else if(ch=='[')
    {
        //totalMsecsControl+=timer.elapsed();
        obj = new JSONArray();
        //obj=dynamic_cast<JSONArray*>(obj);
        obj->parse(json,currentIndex);
        //timer.restart();
    }
    else if(ch=='\"' ||ch=='\'')
    {
        //totalMsecsControl+=timer.elapsed();
        obj = new JSONString();
        //obj=dynamic_cast<JSONString*>(obj);
        obj->parse(json,currentIndex);
        //timer.restart();
    }
    else if((ch>='0' && ch <='9') || ch=='-')
    {
       // totalMsecsControl+=timer.elapsed();
        obj = new JSONNumber();
        //obj=dynamic_cast<JSONNumber*>(obj);
        obj->parse(json,currentIndex);
        //timer.restart();
    }
    else if(ch=='f' || ch=='t')
    {
        //totalMsecsControl+=timer.elapsed();
        obj = new JSONBool();
        //obj=dynamic_cast<JSONValue*>(obj);
        obj->parse(json,currentIndex);
        //timer.restart();
    }
    else if(ch=='n')
    {
        obj=new JSONNullObject;
        obj->parse(json,currentIndex);
    }

    //totalMsecsControl+=timer.elapsed();
   // totalControl++;
    return obj;
}


std::string JSONParser::removeSpaces(std::string& str)
{
    QTime timer;
    timer.start();
    char* c_str= new char[str.size()];
    int c_str_pos=0;
    char currentQuotes=0;
    bool previousScreened=false;
    for(std::string::iterator it=str.begin();it!=str.end();it++)
    {
        //Если мы внутри символов " или '
        if(*it=='\"' || *it=='\'')
        {
            //Запомним это
            if(currentQuotes==0)
            {
                currentQuotes=*it;
            }
            else if(*it==currentQuotes && !previousScreened)
            {
                currentQuotes=0;
            }
        }
        //Если нашли пробел и не внутри строки
        if(std::isspace(*it,std::locale()) && currentQuotes==0)
        {
            //Ничего не делаем
        }
        //Иначе
        else
        {
            //Копируем символ в выходную строку
            c_str[c_str_pos++]=*it;
        }
        if(*it=='\\')
        {
           previousScreened=true;
        }
        else
        {
            previousScreened=false;
        }
    }
    std::string result(c_str);
    delete[] c_str;
    qDebug() << "Removed spaces in" << (double)timer.elapsed()/1000 << "sec" ;
    return result;
}
