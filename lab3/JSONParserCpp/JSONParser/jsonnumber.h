#ifndef JSONNUMBER_H
#define JSONNUMBER_H
#include "jsontype.h"
#include <iostream>
#include <sstream>

class JSONNumber: public JSONType
{
private:
    double val;
public:
    JSONNumber(std::string &str)
    {
        if(str.find('E')!=std::string::npos || str.find('e')!=std::string::npos)
        {
            std::istringstream stream(str);
            stream >> val;
        }
        else
        {
           this->val=::atof(str.c_str());
        }
    }

    void parse(std::string &json, int &currentIndex)
    {
     //   QTime timer;
       // timer.start();
        std::string result;
        char currentChar=json.at(currentIndex);
        while (currentChar != ']' && currentChar != '}' && currentChar != ',')
        {
            result.append(1,currentChar);
            currentIndex++;
            currentChar=json.at(currentIndex);
        }
       // totalMsecsNumber+=timer.elapsed();
        //totalNumber++;
        if(result.find('E')!=std::string::npos || result.find('e')!=std::string::npos)
        {
            std::istringstream stream(result);
            stream >> val;
        }
        else
        {
           this->val=::atof(result.c_str());
        }
    }
    JSONNumber()
    {

    }

    std::string toString()
    {
        std::ostringstream stream;
        stream << val;
        return std::string(stream.str());
    }


    double value()
    {
        return this->val;
    }
};

#endif // JSONNUMBER_H
