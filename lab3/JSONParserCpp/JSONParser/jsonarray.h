#ifndef JSONARRAY_H
#define JSONARRAY_H
#include "jsontype.h"
#include "jsonparser.h"
#include <vector>
class JSONArray : public JSONType
{
private:
    std::vector<JSONType*> elements;
public:
    JSONArray()
    {
    }

    void parse(std::string& json, int &currentIndex);

    ~JSONArray()
    {
        for(std::vector<JSONType*>::iterator it=elements.begin();it!=elements.end();it++)
        {
            delete *it;
        }
        elements.clear();
    }

    JSONType* get(int index)
    {
        return elements.at(index);
    }

    std::string toString()
    {
        std::string result="[";
        for(int i=0;i<elements.size();i++)
        {
            result+=elements.at(i)->toString()+",";
        }
        result+="]";
        return result;
    }

    JSONArray* append(JSONType *element)
    {
        elements.push_back(element);
        return this;
    }
};

#endif // JSONARRAY_H
