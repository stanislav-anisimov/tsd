#ifndef JSONSTRING_H
#define JSONSTRING_H
#include "jsontype.h"

class JSONString: public JSONType
{
private:
    std::string val;
public:
    JSONString()
    {

    }
    JSONString(std::string& str)
    {
        val=str;
    }

    std::string toString()
    {
        return val;
    }
   void parse(std::string& json, int &currentIndex)
    {
        //QTime timer;
        //timer.start();
        std::string result;
        currentIndex++;
        char currentChar=json.at(currentIndex);
        while (currentChar != '\"' && currentChar!='\'')
        {
            if(currentChar=='\\')
            {
                currentIndex++;
                currentChar=json.at(currentIndex);
                switch(currentChar)
                {
                case 'b':
                {
                    result.append(1,'\b');
                    break;
                }
                case 'f':
                {
                    result.append(1,'\f');
                    break;
                }
                case 'n':
                {
                    result.append(1,'\n');
                    break;
                }
                case 'r':
                {
                    result.append(1,'\r');
                    break;
                }
                case 't':
                {
                    result.append(1,'\t');
                    break;
                }
                case 'u'://\u1234
                {
                    std::string buf=json.substr(currentIndex+1,4);
                    char ch=strtol(buf.c_str(),0,16);
                    result.append(1,ch);
                    currentIndex+=4;
                    break;
                }
                default:
                {
                    result.append(1,currentChar);}
                }
            }
            else
            {
                result.append(1,currentChar);
            }
            currentIndex++;
            currentChar=json.at(currentIndex);
        }
        currentIndex++;
        //totalMsecsString+=timer.elapsed();
        //totalString++;
        this->val=result;
    }
    std::string value()
    {
        return val;
    }
};

#endif // JSONSTRING_H
