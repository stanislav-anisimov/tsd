#ifndef JSONVALUE_H
#define JSONVALUE_H
#include "jsontype.h"

class JSONValue:public JSONType
{
private:
    std::string value;
public:
    JSONValue(std::string& value)
    {
        this->value=value;
    }

    void parse(std::string& json, int &currentIndex)
    {
        //QTime timer;
        //timer.start();
        //Если строка в кавычках, копируем ее до закрывающих кавычек. Если без - копируем до ] или } или ,
        std::string result;
        char quotes=json.at(currentIndex);
        if (quotes == '\"' || quotes == '\'')
        {
            currentIndex++;
            result.append(json.substr(currentIndex,json.find(quotes,currentIndex+1)-currentIndex));
            currentIndex=json.find(quotes,currentIndex+1)+1;
        }
        else
        {
            char currentChar=json.at(currentIndex);
            while (currentChar != ']' && currentChar != '}' && currentChar != ',')
            {
                result.append(1,currentChar);
                currentIndex++;
                currentChar=json.at(currentIndex);
            }
        }
        //totalVal++;
        //totalMsecsVal+=timer.elapsed();
        this->value=(result);
    }

    JSONValue()
    {


    }

    std::string toString()
    {
        return value;
    }

    ~JSONValue()
    {

    }
};

#endif // JSONVALUE_H
