#ifndef JSONPARSER_H
#define JSONPARSER_H
#include "jsontype.h"
#include "jsonarray.h"
#include "jsonobject.h"
#include "jsonvalue.h"
#include "jsonstring.h"
#include "jsonnumber.h"
#include "jsonbool.h"
#include "jsonnullobject.h"
#include <locale>
#include <QTime>
#include <QDebug>

class JSONParser
{
private:
    static int currentIndex;
    static int totalMsecsArr;
    static int totalMsecsObj;
    static int totalMsecsVal;
    static int totalMsecsControl;
    static int totalMsecsString;
    static int totalMsecsNumber;

    static int percent;

    static int totalArr;
    static int totalObj;
    static int totalVal;
    static int totalControl;
    static int totalString;
    static int totalNumber;


    static std::string removeSpaces(std::string& str);


public:
    static JSONType* parseController(std::string& json, int &currentIndex);

    JSONParser()
    {
    }
    ~JSONParser()
    {

    }

    static JSONType* parse(std::string& json)
    {
        json=removeSpaces(json);
        JSONType* result=parseController(json,currentIndex);
        qDebug() << "Obj avg" << (double)totalMsecsObj/totalObj << "msec";
        qDebug() << "Arr avg" << (double)totalMsecsArr/totalArr<< "msec";
        qDebug() << "Val avg" << (double)totalMsecsVal/totalVal<< "msec";
        qDebug() << "Controller avg" << (double)totalMsecsControl/totalControl<< "msec";
        qDebug() << "String avg" <<(double)totalMsecsString/totalString<< "msec";
        qDebug() << "Number avg" <<(double)totalMsecsNumber/totalNumber<< "msec";

        qDebug() << "Obj total time" << (double)totalMsecsObj/1000<< "sec";
        qDebug() << "Arr total time" << (double)totalMsecsArr/1000<< "sec";
        qDebug() << "Val total time" << (double)totalMsecsVal/1000<< "sec";
        qDebug() << "Controller total time" << (double)totalMsecsControl/1000<< "sec";
        qDebug() << "String total time" << (double)totalMsecsString/1000<< "sec";
        qDebug() << "Number total time" << (double)totalMsecsNumber/1000<< "sec";


        qDebug() << "Obj total" << (double)totalObj;
        qDebug() << "Arr total" << (double)totalArr;
        qDebug() << "Val total" << (double)totalVal;
        qDebug() << "Controller total" << (double)totalControl;
        qDebug() << "String total" << (double)totalString;
        qDebug() << "Number total" << (double)totalNumber;

        return result;
    }


};

#endif // JSONPARSER_H
