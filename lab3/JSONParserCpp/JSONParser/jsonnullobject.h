#ifndef JSONNULLOBJECT_H
#define JSONNULLOBJECT_H
#include "jsontype.h"

class JSONNullObject : public JSONType
{
public:
    void parse(std::string& json, int &currentIndex)
    {
        currentIndex+=4;
    }
    std::string toString()
    {
        return std::string("NULL");
    }

    JSONNullObject();
};

#endif // JSONNULLOBJECT_H
