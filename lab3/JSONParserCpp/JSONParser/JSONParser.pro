QT += core
QT -= gui

CONFIG += c++11

TARGET = JSONParser
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    jsonparser.cpp \
    jsonobject.cpp \
    jsonarray.cpp \
    jsonbool.cpp \
    jsonnullobject.cpp

HEADERS += \
    jsontype.h \
    jsonvalue.h \
    jsonobject.h \
    jsonarray.h \
    jsonparser.h \
    jsonstring.h \
    jsonnumber.h \
    jsonbool.h \
    jsonnullobject.h
