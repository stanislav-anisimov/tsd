#ifndef JSONTYPE_H
#define JSONTYPE_H
#include "stdfix.h"
//#include "jsonarray.h"
//#include "jsonnumber.h"
//#include "jsonobject.h"
//#include "jsonvalue.h"
#include <string>

class JSONType
{
public:
    JSONType()
    {

    }

    ~JSONType()
    {

    }

    //virtual JSONArray* toArray()=0;
    //virtual JSONNumber* toNumber()=0;
    //virtual JSONObject* toObject()=0;
    //virtual JSONValue* toValue()=0;

    virtual void parse(std::string& json, int &currentIndex)=0;

    virtual std::string toString()=0;
};

#endif // JSONTYPE_H
