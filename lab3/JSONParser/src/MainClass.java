import javafx.util.Pair;

/**
 * Created by sanis on 21.10.2016.
 */
import java.io.*;
public class MainClass
{
	public static void main(String [ ] args)
	{
		JSONObject a= new JSONObject();
		JSONValue val=new JSONValue("1");
		a.append("num",val);
		JSONArray b=new JSONArray();
		val=new JSONValue("2");
		b.append(val);
		val=new JSONValue("3");
		b.append(val);
		val=new JSONValue("4");
		b.append(val);
		val=new JSONValue("5");
		b.append(val);
		a.append("numArr",b);
		JSONObject c=new JSONObject();
		val=new JSONValue("false");
		c.append("bool",val);
		val=new JSONValue("null");
		c.append("nullVal",val);
		b.append(c);
//		System.out.printf(a.toString());
//		String string="a ds s a f \' asd ad w ga \' asd\tfa asd\nfa asdf \" asdf asdf a a\"";
//		System.out.print(string+"\n\n");
//		System.out.print(JSONParser.removeSpaces(string));
		try
		{
			StringBuilder json=new StringBuilder();
			FileInputStream input = new FileInputStream(new File("input.json"));
			int ch;
			while((ch = input.read())!=-1)
			{
				json.append((char)ch);
			}
			System.out.print(JSONParser.parse(json.toString()).toString());
		}
		catch(FileNotFoundException ex)
		{
			System.err.print(ex);
		}
		catch(IOException ex)
		{
			System.err.print(ex);
		}

	}
}
