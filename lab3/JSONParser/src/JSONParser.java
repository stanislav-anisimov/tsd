/**
 * Created by sanis on 21.10.2016.
 */
import java.lang.Character;
public class JSONParser
{
	public static JSONType parse(String json)
	{
		json=removeSpaces(json);
		return parseController(json);
	}
	private static int currentIndex=0;
	/**
	 * Определяет, какой парс метод вызывать
	 * @param json строка json
	 * @return объект JSONObject
	 */
	private static JSONType parseController(String json)
	{
		JSONType obj=null;
		switch (json.charAt(currentIndex))
		{
			case '{':
				obj=parseObject(json);
				break;
			case '[':
				obj=parseArray(json);
				break;
			default:
				obj=parseValue(json);

		}
		return obj;
	}
	public static String removeSpaces(String str)
	{
		boolean inString=false;
		char[] chars=str.toCharArray();
		int index=0;
		for(char ch:chars)
		{
			//Если мы внутри символов " или '
			if(ch=='\"' || ch=='\'')
			{
				//Запомним это
				inString ^= true;
			}
			//Если нашли пробел и не внутри строки
			if(Character.isWhitespace(ch) && !inString)
			{
				//Удаляем этот пробел
				str=str.substring(0,index)+str.substring(index+1);
				index--;
			}
			index++;
		}
		return str;
	}
	public static JSONObject parseObject(String json)
	{
		JSONObject obj=new JSONObject();
		while(currentIndex<json.length() && json.charAt(currentIndex)!='}')
		{
			currentIndex++;
			String key=json.substring(currentIndex,json.indexOf(':',currentIndex));
			currentIndex=json.indexOf(':',currentIndex)+1;
			JSONType value=parseController(json);
			obj.append(key,value);
		}
		currentIndex++;
		return obj;
	}
	public static JSONArray parseArray(String json)
	{
		JSONArray arr=new JSONArray();
		while(currentIndex<json.length() && json.charAt(currentIndex)!=']')
		{
			currentIndex++;
			JSONType value=parseController(json);
			arr.append(value);
		}
		currentIndex++;
		return arr;
	}
	private static JSONValue parseValue(String json)
	{
		//Если строка в кавычках, копируем ее до закрывающих кавычек. Если без - копируем до ] или } или ,
		StringBuilder result = new StringBuilder();
		char quotes=json.charAt(currentIndex);
		if (quotes == '\"' || quotes == '\'')
		{
			currentIndex++;
			result.append(json.substring(currentIndex,json.indexOf(quotes,currentIndex+1)));
			currentIndex=json.indexOf(quotes,currentIndex+1)+1;
		}
		else while (json.charAt(currentIndex) != ']' && json.charAt(currentIndex) != '}' && json.charAt(currentIndex) != ',')
		{
			result.append(json.charAt(currentIndex));
			currentIndex++;
		}
		return new JSONValue(result.toString());
	}
}

