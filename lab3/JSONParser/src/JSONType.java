/**
 * Created by sanis on 21.10.2016.
 */
public abstract class JSONType
{
    public abstract String get(String field);
    public abstract String toString();
}
