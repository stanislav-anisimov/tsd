import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONObject extends JSONType
{
    private HashMap<String, JSONType> fields;
    public String get(String field)
    {
        String result;
        if(fields.containsKey(field))
        {
            result=fields.get(field).toString();
        }
        else
        {
            result="Field \""+field+"\" not found";
        }
        return result;
    }
    public String toString()
    {
        String result="{";
        List<String> keys=new ArrayList<>(fields.keySet());
        for(int i=0;i<keys.size();i++)
        {
            result+="\n\t"+keys.get(i)+" : "+fields.get(keys.get(i)).toString()+",";
        }
        result+="\n}";
        return result;
    }
    public JSONObject append(String field, JSONType value)
    {
        this.fields.put(field,value);
	    return this;
    }
    public JSONObject()
    {
		fields=new HashMap<String,JSONType>();
    }
}
