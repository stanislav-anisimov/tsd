
import java.util.Vector;

public class JSONArray extends JSONType {
    private Vector<JSONType> elements;
    public String get(String field)
    {
        return "Not an object!";
    }
    public String toString()
    {
        String result="\n[";
        for(int i=0;i<elements.size();i++)
        {
            result+="\n\t"+elements.get(i).toString()+",";
        }
        result+="\n]";
        return result;
    }
    public JSONArray append(JSONType element)
    {
        elements.add(element);
	    return this;
    }
    public JSONArray()
    {
		elements=new Vector<>();
    }
}
